#
# For out-of-tree builds:
# make -f /path/to/src/Makefile CFLAGS=... configure
#

ifeq ($(srcdir),)

  srcdir = $(dir $(MAKEFILE_LIST))
  CFLAGS += -O2 -std=c99 -D_GNU_SOURCE
  CFLAGS += -Wall -Wextra -Wno-pointer-sign -g
  PREFIX ?= /usr/local

endif

# Set configuration varialbes here if needed

#CFLAGS += -I/opt/openssl/include
#LDFLAGS += -L/opt/openssl/lib
#LIBS =
#PREFIX = /opt/xi2bind

BINDIR = $(PREFIX)/bin
MANDIR = $(PREFIX)/share/man/man1

all: xi2bind xi2bind.1 evdev_map

xi2bind: $(srcdir)xi2bind.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(srcdir)xi2bind.c -lX11 -lXi $(LIBS)

evdev_map: $(srcdir)evdev_map.c key_names.h
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ -I. $(srcdir)evdev_map.c $(LIBS)

key_names.h:
	echo '#include <linux/input-event-codes.h>' | gcc -E -dM -x c - | \
	perl -ne \
	'if (/^\#define (KEY_(\w+))\s+\S+/) { print "  { $$1, \"$$2\" },\n" }' \
	> key_names.h

xi2bind.1: $(srcdir)README.md
	sed '1,/^## Man page/d; s/^##//' $(srcdir)README.md | pandoc -f markdown - -t man --standalone -o $@

.PHONY: all configure install clean

configure:
	@if [ -e Makefile ]; then echo "Makefile is in the way"; false; fi
	@{ \
	  printf "srcdir = %s\n" $(srcdir) ; \
	  printf "CFLAGS = %s\n" "$(CFLAGS)" ; \
	  printf "LDFLAGS = %s\n" "$(LDFLAGS)" ; \
	  printf "LIBS = %s\n" "$(LIBS)" ; \
	  printf "PREFIX = %s\n" "$(PREFIX)" ; \
	  printf "include \$$(srcdir)Makefile\n" ; \
	} > Makefile

install: xi2bind evdev_map xi2bind.1
	install -D -t $(DESTDIR)$(BINDIR) -m 755 xi2bind
	install -D -t $(DESTDIR)$(BINDIR) -m 755 evdev_map
	install -D -t $(DESTDIR)$(MANDIR) -m 755 xi2bind.1

clean:
	-rm -f xi2bind xi2bind.1 evdev_map key_names.h
