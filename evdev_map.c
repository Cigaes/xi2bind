/*
 * evdev_map -- manipulate evdev keycode tables
 *
 * Nicolas George, 2020-08-03
 * Public domain
 */

#define _XOPEN_SOURCE_EXTENDED 600

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>

#if __BYTE_ORDER == __LITTLE_ENDIAN
# define REVERSE_SCANCODE 1
#endif

typedef struct Key_name {
    unsigned code;
    const char *name;
} Key_name;

static const Key_name key_names[] = {
#include <key_names.h>
};

static void
scancode_to_string(char *out, __u8 *code, int len)
{
#ifdef REVERSE_SCANCODE
    int ic = len - 1, id = -1;
#else
    int ic = 0, id = +1;
#endif
    while (len-- > 0) {
        snprintf(out, 3, "%02x", code[ic]);
        ic += id;
        out += 2;
    }
}

static void
check_device(int dev)
{
    if (dev < 0) {
        fprintf(stderr, "No device opened\n");
        exit(1);
    }
}

static const char *
get_key_by_code(unsigned code)
{
    size_t i;

    for (i = 0; i < sizeof(key_names) / sizeof(*key_names); i++)
        if (key_names[i].code == code)
            return key_names[i].name;
    return NULL;
}

static unsigned
get_key_by_name(const char *name)
{
    size_t i;
    unsigned code, off = 0;

    for (i = 0; i < sizeof(key_names) / sizeof(*key_names); i++)
        if (strcmp(key_names[i].name, name) == 0)
            return key_names[i].code;
    sscanf(name, "%i%n", &code, &off);
    if (off == 0 || name[off] != 0) {
        fprintf(stderr, "Unknown key: %s\n", name);
        exit(1);
    }
    return code;
}

static void
print_keymap(int dev)
{
    struct input_keymap_entry ke;
    char scancode[sizeof(ke.scancode) * 2 + 1];
    const char *name;
    unsigned i;
    int ret;

    check_device(dev);
    for (i = 0; i < 0x10000; i++) {
        ke.index = i;
        ke.flags = INPUT_KEYMAP_BY_INDEX;
        ret = ioctl(dev, EVIOCGKEYCODE_V2, &ke);
        if (ret < 0) {
            if (errno == EINVAL)
                break;
            perror("ioctl(EVIOCGKEYCODE_V2)");
            exit(1);
        }
        if (ke.index != i) {
            fprintf(stderr, "Inconsistency detected: index: %d != %d\n",
                ke.index, i);
            exit(1);
        }
        if (ke.len > sizeof(ke.scancode)) {
            fprintf(stderr, "Inconsistency detected: len: %d > %zd\n",
                ke.len, sizeof(ke.scancode));
            exit(1);
        }
        scancode_to_string(scancode, ke.scancode, ke.len);
        name = get_key_by_code(ke.keycode);
        printf("%5d %s %#10x %s\n", ke.index, scancode, ke.keycode,
            name == NULL ? "?" : name);
    }
    fflush(stdout);
}

static void
set_keycode(int dev, const char *def)
{
    struct input_keymap_entry ke;
    const char *sep;
    int ic, id, off, ret;
    unsigned c, i;

    check_device(dev);
    sep = strchr(def, '=');
    if (sep == NULL ||
        (size_t)(sep - def) > 2 * sizeof(ke.scancode) ||
        (sep - def) % 2 != 0) {
        fprintf(stderr, "Invalid definition: %s\n", def);
        exit(1);
    }
    ke.len = (sep - def) / 2;
#ifdef REVERSE_SCANCODE
    ic = ke.len - 1;
    id = -1;
#else
    ic = 0;
    id = +1;
#endif
    for (i = 0; i < ke.len; i++) {
        off = 0;
        sscanf(def + i * 2, "%02x%n", &c, &off);
        if (off != 2) {
            fprintf(stderr, "Invalid scancode: %s\n", def);
            exit(1);
        }
        ke.scancode[ic] = c;
        ic += id;
    }
    ke.keycode = get_key_by_name(sep + 1);
    ret = ioctl(dev, EVIOCSKEYCODE_V2, &ke);
    if (ret < 0) {
        perror("ioctl(EVIOCSKEYCODE_V2)");
        exit(1);
    }
}

static void
usage(int ret)
{
    FILE *out = ret ? stderr : stdout;

    fprintf(out,
        "evdev_map -- manipulate evdev keycode tables\n"
        "Usage: evdev_map -d device [-p] [-s scancode=keycode]\n"
        "\n"
        "    -d device            select the input device\n"
        "    -p                   print the current map\n"
        "                         columns: index scancode keycode key_name\n"
        "    -s scancode=keycode  change the mapping for a scancode\n"
        "                         (key names work too; use 0x0 for RESERVED)\n"
        "    -h                   print this message\n"
        "Options are processed in order and can be repeated.\n"
        );
    fflush(out);
    if (ret)
        exit(ret);
}

int
main(int argc, char **argv)
{
    int dev = -1, opt;

    while ((opt = getopt(argc, argv, "d:ps:h")) >= 0) {
        switch (opt) {

            case 'd':
                if (dev >= 0)
                    close(dev);
                dev = open(optarg, O_RDONLY);
                if (dev < 0) {
                    perror(optarg);
                    exit(1);
                }
                break;

            case 'p':
                print_keymap(dev);
                break;

            case 's':
                set_keycode(dev, optarg);
                break;

            case 'h':
                usage(0);
                break;

            default:
                usage(1);
                break;
                
        }
    }
    if (optind < argc)
        usage(1);
    return 0;
}
