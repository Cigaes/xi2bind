# xi2bind — react to events from the X input extension

xi2bind is a tool to react to events regarding multiple input devices
connected to an X11 server. Its possible uses include:

- defining a keyboard short cut only on a specific keyboard;

- applying geometry or speed options to a mouse on hot-plug;

- setting a different layout for a keyboard on hot-plug.

Some of these uses can be also done using snippets of configuration for the
X11 server or udev rules, but xi2bind does not require root privileges.

See also <https://lists.debian.org/debian-user/2020/02/msg00755.html>.

## Building

From the source directory: `make`

From another directory: `make -f /path/to/src/Makefile configure && make`

## `edev_map`

This project provides another tool: `evdev_map`, to manipulate the
translation tables of Linux input devices. It allows to achieve different
layouts on the Linux console.

For example, to swap A and Q:

```
evdev_map -d $dev -s 00070004=Q -s 00070014=A
```

See <https://lists.debian.org/debian-user/2020/08/msg00179.html> for
detailed use instructions.

## Man page
% xi2bind(1)

### NAME

xi2bind — react to events from the X input extension

### SYNOPSIS

**xi2bind** [*options*] *directive*…

### DESCRIPTION

xi2bind is a tool to react to events regarding multiple input devices
connected to an X11 server. Its possible uses include:

- defining a keyboard short cut only on a specific keyboard;

- applying geometry or speed options to a mouse on hot-plug;

- setting a different layout for a keyboard on hot-plug.

### SYNTAX

Each *directive* argument defines a binding or an option for subsequent
directives.

**device** *device-id-or-name*
: Set the device used by the next directives. Can be a number or the name of
a device. TODO: allow to select keyboards/mice by name.

**ignore_mod** *modifiers*
: Set the ignored modifiers. The default is **lock** (caps lock) and
**mod2** (usually num lock). See below the syntax for the keys for the name
of the codes for the modifiers. Use `/` to mean no ignored modifiers.

**bind** *key* *command*
: Define a binding / keyboard shortcut on the specified key combination on
the current device to execute the given command. If key is `*`, the binding
grabs the whole device but the command is executed only for keys events.

**hierarchy** *command*
: Execute the given command when the hierarchy of devices changes,
especially when a device is added or removed.

#### Keys

A key can be specified by its keycode between angle brackets. For example
`<65>` is the space bar on PC keyboards. Or it can be specified by a keysym
attached to it **on the default layout and ignoring modifiers (TODO)**.

Modifiers can be specified by prefixing them to the key with a dash;
multiple specifiers can be added, each with a dash, in any order. For
example `C-S-<65>` is space with control and shift pressed. The codes for
the modifiers are:

- **S**: shift
- **L**: lock (usually caps lock)
- **C**: control
- **1**/**A**: mod1 (usually alt)
- **2**/**N**: mod2 (usually num lock)
- **3**/**U**: mod3 (usually super)
- **4**/**M**: mod4 (usually meta)
- **5**/**G**: mod5 (usually alt gr)

#### Commands execution

Commands are executed using `$SHELL -c`, or `/bin/sh` if `$SHELL` is not
defined.

For hierarchy events, the following environment variables are defined for
the command:

- **DEVICE**: numeric id of the device
- **DEVICE_NAME**: human-readable name of the device
- **ENABLED**: 1 if the device is enabled, 0 otherwise
- **ATTACHMENT**: numeric id of the attachment device
- **FLAG_MASTER_ADDED**: 0 or 1
- **FLAG_MASTER_REMOVED**: 0 or 1
- **FLAG_SLAVE_ADDED**: 0 or 1
- **FLAG_SLAVE_REMOVED**: 0 or 1
- **FLAG_SLAVE_ATTACHED**: 0 or 1
- **FLAG_SLAVE_DETACHED**: 0 or 1
- **FLAG_DEVICE_ENABLED**: 0 or 1
- **FLAG_DEVICE_DISABLED**: 0 or 1
- **USE**: `master_pointer` or `master_keyboard` or `slave_pointer` or
  `slave_keyboard` or `floating_slave` or `unknown_`*x*

### EXAMPLES

Go to the default desktop when the “homepage” key is pressed on a model of
wireless keyboard:

```
xi2bind 'device Logitech K400' 'bind <180> FvwmCommand "GotoDeskAndPage 0 0 0"'
```

Apply a speed option to a hot-plugged mouse:

```
xi2bind hierarchy 'case "$DEVICE_NAME" in *Optical*Mouse) xinput set-prop "$DEVICE" "libinput Accel Speed" -0.4;; esac'
```

Grab a specific device and ignore everything (could be achieved with `xinput
disable`, except if bindings are defined on specific keys too):

```
xi2bind 'device 10' 'bind * :'
```

### SEE ALSO

**xinput**(1), **xev**(1)

### AUTHOURS

Nicolas George <george@nsup.org>
