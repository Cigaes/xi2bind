/*
 * xi2bind - bind keys to specific input devices
 * (c) 2022 Nicolas George
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#define _XOPEN_SOURCE 600
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/extensions/XInput2.h>

typedef struct Binding {
    const char *cmd;
    int device;
    int keycode;
    unsigned char mod;
    unsigned char mod_ignore;
} Binding;

typedef struct Substr {
    const char *s;
    const char *e;
} Substr;

typedef struct Parse_state {
    int device;
    uint8_t mod_ignore;
} Parse_state;

typedef struct Modifer_name {
    char name;
    uint8_t val;
} Modifer_name;

static Display *display;
static Binding *bindings = NULL;
static unsigned nb_bindings = 0;
static const char *hierarchy_cmd = NULL;
static const char *control_prop = /*"XI2BIND"*/ NULL;
static Atom control_atom;

static inline Substr
substr_from_string(const char *str)
{
    return (Substr){ .s = str, .e = str + strlen(str) };
}

static inline size_t
substr_len(Substr s)
{
    return s.e - s.s;
}

static void
substr_first_word(Substr str, Substr *kw, Substr *rest)
{
    const char *p;

    p = str.s;
    while (*p != 0 && *p != ' ' && *p != '\t')
        p++;
    kw->s = str.s;
    kw->e = p;
    while (*p == ' ' || *p == '\t')
        p++;
    rest->s = p;
    rest->e = str.e;
}

static int
substr_match(Substr str, const char *ref)
{
    size_t l = strlen(ref);

    return str.e == str.s + l && memcmp(ref, str.s, l) == 0;
}

static char *
substr_dup(Substr str)
{
    size_t s = substr_len(str) + 1;
    char *buf = malloc(s);
    if (buf == NULL) {
        perror("malloc");
        exit(1);
    }
    memcpy(buf, str.s, s);
    return buf;
}

#define substr_printf(str) (int)(str.e - str.s), str.s

static void
select_events(void)
{
    XIEventMask mask = { 0 };
    unsigned char rmask[XIMaskLen(XI_LASTEVENT)] = { 0 };
    Window root = DefaultRootWindow(display);

    if (hierarchy_cmd != NULL) {
        mask.deviceid = XIAllDevices;
        mask.mask_len = XIMaskLen(XI_LASTEVENT);
        mask.mask = rmask;
        XISetMask(mask.mask, XI_HierarchyChanged);
        XISelectEvents(display, root, &mask, 1);
    }
    if (control_prop != NULL) {
        XSelectInput(display, root, PropertyChangeMask);
        control_atom = XInternAtom(display, control_prop, 0);
    }
}

static void
set_info(XIHierarchyInfo *info, XIDeviceInfo *devices, int nb_devices)
{
    char idbuf[32], atbuf[32], typebuf[32], *type;
    int i;

    snprintf(idbuf, sizeof(idbuf), "%d", info->deviceid);
    snprintf(atbuf, sizeof(atbuf), "%d", info->attachment);
    setenv("DEVICE", idbuf, 1);
    for (i = 0; i < nb_devices; i++) {
        if (devices[i].deviceid == info->deviceid) {
            setenv("DEVICE_NAME", devices[i].name, 1);
            break;
        }
    }
    setenv("ENABLED", info->enabled ? "1" : "0", 1);
    setenv("ATTACHMENT", atbuf, 1);
    #define ENV_FLAG(e, f) \
        setenv("FLAG_" e, (info->flags & f) ? "1" : "0", 1)
    ENV_FLAG("MASTER_ADDED", XIMasterAdded);
    ENV_FLAG("MASTER_REMOVED", XIMasterRemoved);
    ENV_FLAG("SLAVE_ADDED", XISlaveAdded);
    ENV_FLAG("SLAVE_REMOVED", XISlaveRemoved);
    ENV_FLAG("SLAVE_ATTACHED", XISlaveAttached);
    ENV_FLAG("SLAVE_DETACHED", XISlaveDetached);
    ENV_FLAG("DEVICE_ENABLED", XIDeviceEnabled);
    ENV_FLAG("DEVICE_DISABLED", XIDeviceDisabled);
    switch (info->use) {
        case 0: type = "none"; break;
        case XIMasterPointer: type = "master_pointer"; break;
        case XIMasterKeyboard: type = "master_keyboard"; break;
        case XISlavePointer: type = "slave_pointer"; break;
        case XISlaveKeyboard: type = "slave_keyboard"; break;
        case XIFloatingSlave: type = "floating_slave"; break;
        default:
            snprintf(typebuf, sizeof(typebuf), "unknown_%d", info->use);
            type = typebuf;
            break;
    }
    setenv("USE", type, 1);
}

static void
run_command(const char *cmd, XIHierarchyInfo *info)
{
    XIDeviceInfo *devices = NULL;
    pid_t child;
    int nb_devices = 0;

    if (info != NULL)
        devices = XIQueryDevice(display, 0, &nb_devices);
    child = fork();
    if (child < 0) {
        perror("fork");
        return;
    }
    if (child == 0) {
        const char *shell = getenv("SHELL");
        if (shell == NULL)
            shell = "/bin/sh";
        if (info != NULL)
            set_info(info, devices, nb_devices);
        execl(shell, shell, "-c", cmd, NULL);
        perror("exec");
        _exit(1);
    }
    if (devices != NULL)
        XIFreeDeviceInfo(devices);
}

static int
find_device(Substr dev)
{
    int ret, n = 0, i;
    XIDeviceInfo *info;

    if (sscanf(dev.s, "%d%n", &ret, &n) >= 1 && n > 0 && dev.e == dev.s + n)
        return ret;
    info = XIQueryDevice(display, XIAllDevices, &n);
    ret = 0;
    for (i = 0; i < n; i++) {
        if (info[i].use != XIMasterKeyboard &&
            info[i].use != XISlaveKeyboard)
            continue;
        if (substr_match(dev, info[i].name)) {
            ret = info[i].deviceid;
            break;
        }
    }
    XIFreeDeviceInfo(info);
    if (ret == 0)
        fprintf(stderr, "Device not found: %.*s\n", substr_printf(dev));
    return ret;
}

static void
add_binding(int device, unsigned mod_ignore, unsigned mod, int keycode,
    Substr cmd)
{
    Binding *b;

    if ((nb_bindings & (nb_bindings + 1)) == 0) {
        bindings = realloc(bindings, (nb_bindings * 2 + 1) * sizeof(*bindings));
        if (bindings == NULL) {
            perror("malloc");
            exit(1);
        }
    }
    b = &bindings[nb_bindings];
    b->device = device;
    b->keycode = keycode;
    b->mod = mod;
    b->mod_ignore = mod_ignore;
    b->cmd = substr_dup(cmd);
    nb_bindings++;
}

static int
grab_keycode(int device, unsigned mod_ignore, unsigned mod, int keycode,
    Substr cmd)
{
    XIEventMask mask = { 0 };
    unsigned char rmask[XIMaskLen(XI_LASTEVENT)] = { 0 };
    XIGrabModifiers gmod[256];
    Window window = DefaultRootWindow(display);
    unsigned i, nmod = 0;
    int ret;

    mask.deviceid = XIAllDevices;
    mask.mask_len = XIMaskLen(XI_LASTEVENT);
    mask.mask = rmask;
    for (i = 0; i < 256; i++) {
        if ((i & ~mod_ignore) != 0)
            continue;
        gmod[nmod].modifiers = i | mod;
        gmod[nmod].status = 0;
        nmod++;
    }
    ret = XIGrabKeycode(display, device, keycode, window,
        XIGrabModeAsync, XIGrabModeAsync, 0, &mask, nmod, gmod);
    if (ret != 0) {
        fprintf(stderr, "Failed to grab key\n");
        return -1;
    }
    add_binding(device, mod_ignore, mod, keycode, cmd);
    return 1;
}

static int
grab_device(int device, unsigned mod_ignore, Substr cmd)
{
    XIEventMask mask = { 0 };
    unsigned char rmask[XIMaskLen(XI_LASTEVENT)] = { 0 };
    int ret;

    mask.deviceid = XIAllDevices;
    mask.mask_len = XIMaskLen(XI_LASTEVENT);
    mask.mask = rmask;
    XISetMask(mask.mask, XI_KeyPress);
    ret = XIGrabDevice(display, device, DefaultRootWindow(display),
        CurrentTime, None, XIGrabModeAsync, XIGrabModeAsync, 0, &mask);
    if (ret != 0) {
        fprintf(stderr, "Failed to grab device\n");
        return -1;
    }
    add_binding(device, mod_ignore, 0, 0, cmd);
    return 1;
}

static KeySym
substr_to_keysym(Substr k)
{
    /* ISO_Discontinuous_Underline and Greek_upsilonaccentdieresis
       are the longest */
    char buf[32];
    size_t l = substr_len(k);

    if (l >= sizeof(buf))
        l = sizeof(buf) - 1;
    memcpy(buf, k.s, l);
    buf[l] = 0;
    return XStringToKeysym(buf);
}

static unsigned
mod_char_to_mask(char l)
{
    static const Modifer_name mod_map[] = {
        { 'S', ShiftMask   },
        { 'L', LockMask    },
        { 'C', ControlMask },
        { '1', Mod1Mask    },
        { 'A', Mod1Mask    },
        { '2', Mod2Mask    },
        { 'N', Mod2Mask    },
        { '3', Mod3Mask    },
        { 'U', Mod3Mask    },
        { '4', Mod4Mask    },
        { 'M', Mod4Mask    },
        { '5', Mod5Mask    },
        { 'G', Mod5Mask    },
    };

    unsigned i;
    for (i = 0; i < sizeof(mod_map) / sizeof(*mod_map); i++)
        if (mod_map[i].name == l)
            return mod_map[i].val;
    fprintf(stderr, "Unknown modifier: %c\n", l);
    return 0;
}


static int
grab_key(int device, unsigned mod_ignore, Substr keyname, Substr cmd)
{
    unsigned mod = 0, nmod;
    int off, keycode;
    KeySym keysym;

    if (keyname.e == keyname.s + 1 && keyname.s[0] == '*')
        return grab_device(device, mod_ignore, cmd);

    while (keyname.e > keyname.s + 2 && keyname.s[1] == '-') {
        nmod = mod_char_to_mask(keyname.s[0]);
        if (nmod == 0)
            return -1;
        mod |= nmod;
        keyname.s += 2;
    }
    if ((mod & mod_ignore) != 0) {
        fprintf(stderr, "Key modifiers are ignored\n");
        return -1;
    }
    off = 0;
    if (sscanf(keyname.s, "<%d>%n", &keycode, &off) >= 1 &&
        off > 0 && keyname.e == keyname.s + off) {
        return grab_keycode(device, mod_ignore, mod, keycode, cmd);
    }
    keysym = substr_to_keysym(keyname);
    if (keysym == 0) {
        fprintf(stderr, "Unknown key name: %.*s\n", substr_printf(keyname));
        return -1;
    }
    keycode = XKeysymToKeycode(display, keysym); /* FIXME use device layout */
    if (keycode == 0) {
        fprintf(stderr, "Key not found: %.*s\n", substr_printf(keyname));
        return -1;
    }
    return grab_keycode(device, mod_ignore, mod, keycode, cmd);
}

static int
parse_line(Parse_state *state, Substr line)
{
    Substr kw, arg1, rest;

    substr_first_word(line, &kw, &rest);

    if (substr_match(kw, "device")) {
        if (rest.e == rest.s) {
            fprintf(stderr, "Directive device requires an argument\n");
            return -1;
        }
        state->device = find_device(rest);
        if (state->device == 0)
            return -1;
        return 1;
    }

    if (substr_match(kw, "bind")) {
        substr_first_word(rest, &arg1, &rest);
        if (rest.e == rest.s) {
            fprintf(stderr, "Directive bind requires arguments\n");
            return -1;
        }
        return grab_key(state->device, state->mod_ignore, arg1, rest);
    }

    if (substr_match(kw, "ignore_mod")) {
        if (rest.e == rest.s) {
            fprintf(stderr, "Directive ignore requires an argument\n");
            return -1;
        }
        if (rest.e == rest.s + 1 && rest.s[0] == '/') {
            state->mod_ignore = 0;
        } else {
            unsigned mod = 0, nmod;
            while (rest.s < rest.e) {
                nmod = mod_char_to_mask(rest.s[0]);
                if (nmod == 0)
                    return -1;
                mod |= nmod;
                rest.s++;
            }
            state->mod_ignore = mod;
        }
        return 1;
    }

    if (substr_match(kw, "hierarchy")) {
        if (rest.e == rest.s) {
            fprintf(stderr, "Directive hierarchy requires arguments\n");
            return -1;
        }
        hierarchy_cmd = substr_dup(rest);
        return 1;
    }

    fprintf(stderr, "Unknown directive: %.*s\n", substr_printf(kw));
    return -1;
}

#define PARSE_STATE_INIT { .device = 0, .mod_ignore = LockMask | Mod2Mask }

static void
parse_arguments(int argc, char **argv)
{
    Parse_state state = PARSE_STATE_INIT;
    int i, ret;

    for (i = 0; i < argc; i++) {
        ret = parse_line(&state, substr_from_string(argv[i]));
        if (ret < 0)
            exit(1);
    }
}

static void
process_keypress(XIDeviceEvent *ev)
{
    unsigned i;
    Binding *b;

    for (i = 0; i < nb_bindings; i++) {
        b = &bindings[i];
        if (b->keycode == 0)
            continue;
        if (ev->deviceid != bindings[i].device ||
            (ev->mods.effective & ~b->mod_ignore) != b->mod ||
            ev->detail != b->keycode)
            continue;
        run_command(b->cmd, NULL);
        return;
    }
    for (i = 0; i < nb_bindings; i++) {
        b = &bindings[i];
        if (b->keycode != 0)
            continue;
        if (ev->deviceid != bindings[i].device)
            continue;
        run_command(b->cmd, NULL);
        return;
    }
}

static void
process_hierarchy(XIHierarchyEvent *ev)
{
    int i;
    XIHierarchyInfo *info;

    for (i = 0; i < ev->num_info; i++) {
        info = &ev->info[i];
        if (info->flags != 0)
            run_command(hierarchy_cmd, info);
    }
}

int
main(int argc, char **argv)
{
    XEvent ev;
    int xi_major, xi_event, xi_error;

    signal(SIGCHLD, SIG_IGN);
    display = XOpenDisplay(NULL);
    if (display == NULL) {
        fprintf(stderr, "Unable to open display\n");
        exit(1);
    }
    if (!XQueryExtension(display, "XInputExtension",
        &xi_major, &xi_event, &xi_error)) {
        fprintf(stderr, "XI2 not available\n");
        exit(1);
    }
    parse_arguments(argc - 1, argv + 1);
    select_events();
    while (1) {
        XNextEvent(display, &ev);
        if (ev.type == GenericEvent &&
            ev.xcookie.extension == xi_major &&
            XGetEventData(display, &ev.xcookie)) {
            if (ev.xcookie.evtype == XI_KeyPress)
                process_keypress(ev.xcookie.data);
            if(ev.xcookie.evtype == XI_HierarchyChanged)
                process_hierarchy(ev.xcookie.data);
        }
    }
    return 0;
}
